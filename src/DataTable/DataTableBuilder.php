<?php

namespace Mediapress\DataTable;

use Yajra\Datatables\Html\Builder;
use  Yajra\Datatables\Html\Parameters;
class DataTableBuilder extends Builder
{
    public function parameterize($attributes = [])
    {
        $lang  = 'tr';
        $data = json_decode(file_get_contents(__DIR__.'/../../locale/'.$lang.'.json'),1);

        $parameters = (new Parameters($attributes))->toArray();
        $parameters['language'] = $data;
        list($columnFunctions, $parameters) = $this->encodeColumnFunctions($parameters);
        list($callbackFunctions, $parameters) = $this->encodeCallbackFunctions($parameters);

        $json = json_encode($parameters);

        $json = $this->decodeColumnFunctions($columnFunctions, $json);
        $json = $this->decodeCallbackFunctions($callbackFunctions, $json);

        return $json;
    }


}