<?php

namespace Mediapress\DataTable;

use  Yajra\Datatables\Datatables as BaseDatatables;

class DataTables extends BaseDatatables
{

    function __construct()
    {
        $request = new Request();
        parent::__construct($request);
    }
}