<?php

namespace Mediapress\DataTable;

use Illuminate\Support\ServiceProvider;
use  Yajra\Datatables\DatatablesServiceProvider;

class DataTableServiceProvider extends ServiceProvider
{
    public function boot()
    {

    }

    public function register()
    {
        $this->app->register(DatatablesServiceProvider::class);
    }
}